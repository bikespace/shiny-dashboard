#!/bin/sh

# Make sure the directory for individual app logs exists
mkdir -p /var/log/shiny-server
chown shiny.shiny /var/log/shiny-server

# Pass the maps API key in
env | grep MAPS_API_KEY > home/shiny/.Renviron
chown shiny:shiny home/shiny/.Renviron

exec shiny-server >> /var/log/shiny-server.log 2>&1